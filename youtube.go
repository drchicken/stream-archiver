package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"regexp"
	"net/url"
	"github.com/MercuryEngineering/CookieMonster"
)

type YTThumbnail struct {
	URL        string `json:"url"`
	Preference int    `json:"preference"`
	ID         string `json:"id"`
}

var (
	reYtIsLive = regexp.MustCompile("{\"text\":\" watching\"}")

	ytHttpClient http.Client
)

func setupYouTube() {
	jar, err := cookiejar.New(nil)
	if err != nil {
		panic(err)
	}

	ytHttpClient = http.Client{
		Jar: jar,
	}

	// check if we should send YouTube cookies
	if config.YouTubeCookieFile != "" {
		cookies, err := cookiemonster.ParseFile(config.YouTubeCookieFile)
		if err != nil {
			panic(err)
		}

		urlObj, _ := url.Parse("https://youtube.com")
		ytHttpClient.Jar.SetCookies(urlObj, cookies)
	}
}

func ytIsChannelLive(id string) (bool, error) {
	resp, err := ytHttpClient.Get(fmt.Sprintf("https://www.youtube.com/channel/%s", id))
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}

	return reYtIsLive.Match(respBytes), nil
}
