# stream-archiver
> A dumb tool to save twitch/youtube livestreams as they happen

# Features
* Watching a configurable set of YouTube/Twitch channels for new streams
* Automatic downloading of new streams
* Automatic retries if a download fails
* Easy (enough) configuration with a toml file
* Hook system for notification, automatic uploads, etc.

# Requirements
* A recent Go version (built with 1.17)
* A Twitch developer account
	* Twitch App Client ID
	* Twitch App Token
* [yt-dlp](https://github.com/yt-dlp/yt-dlp) installed and in your PATH
* [steamlink](https://github.com/streamlink/streamlink) installed and in your PATH
* A Linux system (maybe, haven't tested on Windows)

# Usage
* Copy `config.example.toml` to `config.toml`
  * You can add more channels by adding a new entry in this file
  * The channel name is only used for identifying the channel in logs
* Build the program with `go build`
* Run the program `./stream-archiver`
* For more information, consult [DOCS.md](DOCS.md)

# Wishlist / To-do
* Support for private (members-only) YT streams
* Stream filtering
	* For example, only download streams with "members only" in title