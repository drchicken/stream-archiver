package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"time"
)

func onDownloadStart(meta VideoMetadata) {
	env := []string{
		"EVENT=DOWNLOAD_START",
		fmt.Sprintf("VIDEO_ID=%s", meta.ID),
		fmt.Sprintf("VIDEO_TITLE=%s", meta.Title),
		fmt.Sprintf("VIDEO_THUMBNAIL_URL=%s", meta.Thumbnail),
		fmt.Sprintf("VIDEO_UPLOADER=%s", path.Clean(meta.Uploader)),
		fmt.Sprintf("VIDEO_UPLOADER_ID=%s", meta.UploaderID),
		fmt.Sprintf("VIDEO_FILENAME=%s", path.Clean(meta.Filename)),
		fmt.Sprintf("VIDEO_PLATFORM=%s", meta.Platform),
		fmt.Sprintf("VIDEO_URL=%s", meta.StreamURL),
	}

	executeHook(env, meta.UploaderID)
}

func onDownloadFail(meta VideoMetadata, message string) {
	env := []string{
		"EVENT=DOWNLOAD_FAIL",
		fmt.Sprintf("ERROR_MESSAGE=%s", message),
		fmt.Sprintf("VIDEO_ID=%s", meta.ID),
		fmt.Sprintf("VIDEO_TITLE=%s", meta.Title),
		fmt.Sprintf("VIDEO_THUMBNAIL_URL=%s", meta.Thumbnail),
		fmt.Sprintf("VIDEO_UPLOADER=%s", path.Clean(meta.Uploader)),
		fmt.Sprintf("VIDEO_UPLOADER_ID=%s", meta.UploaderID),
		fmt.Sprintf("VIDEO_FILENAME=%s", path.Clean(meta.Filename)),
		fmt.Sprintf("VIDEO_PLATFORM=%s", meta.Platform),
		fmt.Sprintf("VIDEO_URL=%s", meta.StreamURL),
	}

	executeHook(env, meta.UploaderID)
}

func onDownloadRetry(meta VideoMetadata, try int) {
	env := []string{
		"EVENT=DOWNLOAD_RETRY",
		fmt.Sprintf("RETRY_COUNT=%d", try+1),
		fmt.Sprintf("VIDEO_ID=%s", meta.ID),
		fmt.Sprintf("VIDEO_TITLE=%s", meta.Title),
		fmt.Sprintf("VIDEO_THUMBNAIL_URL=%s", meta.Thumbnail),
		fmt.Sprintf("VIDEO_UPLOADER=%s", path.Clean(meta.Uploader)),
		fmt.Sprintf("VIDEO_UPLOADER_ID=%s", meta.UploaderID),
		fmt.Sprintf("VIDEO_FILENAME=%s", path.Clean(meta.Filename)),
		fmt.Sprintf("VIDEO_PLATFORM=%s", meta.Platform),
		fmt.Sprintf("VIDEO_URL=%s", meta.StreamURL),
	}

	executeHook(env, meta.UploaderID)
}

func onDownloadMetaFail(channel Channel, message string) {
	env := []string{
		"EVENT=DOWNLOAD_META_FAIL",
		fmt.Sprintf("CHANNEL_NAME=%s", channel.Name),
		fmt.Sprintf("CHANNEL_ID=%s", channel.ChannelID),
		fmt.Sprintf("CHANNEL_TYPE=%s", channel.Type),
		fmt.Sprintf("ERROR_MESSAGE=%s", message),
	}

	executeHook(env, channel.ChannelID)
}

func onDownloadFinish(meta VideoMetadata) {
	env := []string{
		"EVENT=DOWNLOAD_FINISH",
		fmt.Sprintf("VIDEO_ID=%s", meta.ID),
		fmt.Sprintf("VIDEO_TITLE=%s", meta.Title),
		fmt.Sprintf("VIDEO_THUMBNAIL_URL=%s", meta.Thumbnail),
		fmt.Sprintf("VIDEO_UPLOADER=%s", path.Clean(meta.Uploader)),
		fmt.Sprintf("VIDEO_UPLOADER_ID=%s", meta.UploaderID),
		fmt.Sprintf("VIDEO_PLATFORM=%s", meta.Platform),
		fmt.Sprintf("VIDEO_FILENAME=%s", meta.Filename),
		fmt.Sprintf("FILE_FULLPATH=%s", meta.FilePath),
		fmt.Sprintf("FILE_THUMBNAIL=%s", meta.ThumbnailPath),
		fmt.Sprintf("VIDEO_URL=%s", meta.StreamURL),
	}

	executeHook(env, meta.UploaderID)
}

func onStreamStart(channel Channel) {
	env := []string{
		"EVENT=STREAM_START",
		fmt.Sprintf("CHANNEL_NAME=%s", channel.Name),
		fmt.Sprintf("CHANNEL_ID=%s", channel.ChannelID),
		fmt.Sprintf("CHANNEL_TYPE=%s", channel.Type),
	}

	executeHook(env, channel.ChannelID)
}

func onStreamEnd(channel Channel) {
	env := []string{
		"EVENT=STREAM_END",
		fmt.Sprintf("CHANNEL_NAME=%s", channel.Name),
		fmt.Sprintf("CHANNEL_ID=%s", channel.ChannelID),
		fmt.Sprintf("CHANNEL_TYPE=%s", channel.Type),
	}

	executeHook(env, channel.ChannelID)
}

func onStreamHiccup(channel Channel) {
	env := []string{
		"EVENT=STREAM_HICCUP",
		fmt.Sprintf("CHANNEL_NAME=%s", channel.Name),
		fmt.Sprintf("CHANNEL_ID=%s", channel.ChannelID),
		fmt.Sprintf("CHANNEL_TYPE=%s", channel.Type),
	}

	executeHook(env, channel.ChannelID)
}

func executeHook(env []string, channelID string) {
	if !config.PostHookEnable {
		return
	}

	cmd := exec.Command(config.PostHook)
	cmd.Env = append(os.Environ(), env...)

	logFile, err := os.Create(fmt.Sprintf("./logs/hook-%s-%d.log", channelID, time.Now().Unix()))
	if err != nil {
		log.Printf("[hook] failed to open log file: %v\n", err)
		return
	}

	cmd.Stdout = logFile
	cmd.Stderr = logFile

	err = cmd.Run()
	if err != nil {
		panic(err)
	}
}
