package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os/exec"
	"time"
)

type VideoMetadata struct {
	ID            string        `json:"id"`
	Title         string        `json:"title"`
	Thumbnail     string        `json:"thumbnail"`
	Thumbnails    []YTThumbnail `json:"thumbnails"`
	Uploader      string        `json:"uploader"`
	UploaderID    string        `json:"uploader_id"`
	IsLive        bool          `json:"is_live"`
	Filename      string        `json:"filename"`
	StreamURL     string        `json:"webpage_url"`
	FilePath      string
	ThumbnailPath string
	Platform      string
}

func getVideoMetadata(channel Channel, timeout time.Duration) (VideoMetadata, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	var url string
	if channel.Type == "youtube" {
		url = fmt.Sprintf("https://www.youtube.com/channel/%s/live", channel.ChannelID)
	} else if channel.Type == "twitch" {
		url = fmt.Sprintf("https://www.twitch.tv/%s", channel.ChannelID)
	} else {
		panic("unkown channel type")
	}

	// use cookies when fetching video metadata too
	var cmd *exec.Cmd
	if channel.Type == "youtube" && config.YouTubeCookieFile != "" {
		cmd = exec.CommandContext(ctx, "yt-dlp", "--cookies", config.YouTubeCookieFile, "-o", "%(upload_date)s - %(uploader)s - %(title)s.%(ext)s", "-j", url)
	} else {
		cmd = exec.CommandContext(ctx, "yt-dlp", "-o", "%(upload_date)s - %(uploader)s - %(title)s.%(ext)s", "-j", url)
	}

	out, err := cmd.Output()
	if err != nil {
		return VideoMetadata{}, err
	}

	if ctx.Err() != nil {
		return VideoMetadata{}, ctx.Err()
	}

	var meta VideoMetadata
	err = json.Unmarshal(out, &meta)
	if err != nil {
		return VideoMetadata{}, err
	}

	// extra bits required to extract thumbnail from youtube
	if channel.Type == "youtube" {
		best := -999
		// keep track of the "most prefferred" thumbnail
		for i := 0; i < len(meta.Thumbnails); i++ {
			if meta.Thumbnails[i].Preference > best {
				meta.Thumbnail = meta.Thumbnails[i].URL
			}
		}

		meta.Thumbnails = make([]YTThumbnail, 0)
		meta.Platform = "youtube"
	} else {
		meta.Platform = "twitch"
	}

	return meta, nil

}
