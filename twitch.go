package main

import (
	"errors"
	"github.com/nicklaw5/helix"
	"log"
)

var (
	twitch *helix.Client
)

type StreamlinkMetadataContainer struct {
	 Metadata StreamlinkMetadata`json:"metadata"`
}

type StreamlinkMetadata struct {
	Author string `json:"author"`
	Category string	`json:"category"`
	Title string `json:"title"`
}

func setupTwitch(config Config) {
	var err error

	twitch, err = helix.NewClient(&helix.Options{
		ClientID:       config.TwitchClientID,
		AppAccessToken: config.TwitchAppToken,
	})
	if err != nil {
		log.Fatalf("[twitch] failed to set up api client: %v\n", err)
	}

	valid, resp, err := twitch.ValidateToken(config.TwitchAppToken)
	if err != nil {
		log.Fatalf("[twitch] failed to veryify app access token: %v\n", err)
	}

	if !valid || resp.StatusCode != 200 {
		log.Fatalf("[twitch] app access token is not valid: %s\n", resp.Error)
	}
}

// func twGetFilename(id string) (string, error) {
// 	// cmd := exec.Command(name, arg)
// 	resp, err := twitch.GetStreams(&helix.StreamsParams{
// 		UserLogins: []string{id},
// 	})

// 	if err != nil {
// 		return "", err
// 	}

// 	if resp.StatusCode != 200 {
// 		return "", errors.New("twitch api returned not 200")
// 	}

// 	cleanTitle := strings.ReplaceAll(resp.Data.Streams[0].Title, "/", "_")

// 	return fmt.Sprintf("%s - %s - %s.mp4", time.Now().Format("20060102"), resp.Data.Streams[0].UserLogin, cleanTitle), nil

// }

func twGetMetadata(id string) (*helix.Stream, error) {
	resp, err := twitch.GetStreams(&helix.StreamsParams{
		UserLogins: []string{id},
	})

	if err != nil {
		return &helix.Stream{}, err
	}

	if resp.StatusCode != 200 {
		return &helix.Stream{}, errors.New("twitch api returned not 200")
	}

	return &resp.Data.Streams[0], nil
}

func twIsChannelLive(id string) (bool, error) {
	resp, err := twitch.GetStreams(&helix.StreamsParams{
		UserLogins: []string{id},
	})
	if err != nil {
		return false, err
	}

	if resp.StatusCode != 200 {
		return false, errors.New("twitch api returned not 200")
	}

	return len(resp.Data.Streams) >= 1, nil
}
