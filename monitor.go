package main

import (
	"log"
)

var (
	monitorStatus = make(map[string]Status)

	// keep track of if a download is running for a channel
	downloadingStatus = make(map[string]bool)
)

type Status struct {
	Live    bool
	// Updated time.Time
}

func monitor(config Config) {
	for i := 0; i < len(config.Channels); i++ {
		var live bool
		var err error

		if config.Channels[i].Type == "youtube" {
			live, err = ytIsChannelLive(config.Channels[i].ChannelID)
		} else if config.Channels[i].Type == "twitch" {
			live, err = twIsChannelLive(config.Channels[i].ChannelID)
		} else {
			panic("unknown channel type")
		}

		if err != nil {
			log.Printf("[%s] failed to check live state for %s: %v\n", config.Channels[i].Type, config.Channels[i].Name, err)
		}

		if live && !monitorStatus[config.Channels[i].ChannelID].Live {
			// stream has started
			log.Printf("[%s] %s (%s) has gone live", config.Channels[i].Type, config.Channels[i].Name, config.Channels[i].ChannelID)
			onStreamStart(config.Channels[i])
			go download(config.Channels[i], config.OutputDir, 0)
		} else if !live && monitorStatus[config.Channels[i].ChannelID].Live {
			// stream has ended
			log.Printf("[%s] %s (%s) is no longer live", config.Channels[i].Type, config.Channels[i].Name, config.Channels[i].ChannelID)
			onStreamEnd(config.Channels[i])
		} else if live && !downloadingStatus[config.Channels[i].ChannelID] {
			// stream reported as live, but the download has stopped or isn't running
			// TODO: a mechanism for not retrying failed downloads (or at least implementing a proper delay)
			log.Printf("[%s] %s (%s) possible stream interruption?", config.Channels[i].Type, config.Channels[i].Name, config.Channels[i].ChannelID)
			onStreamHiccup(config.Channels[i])
			go download(config.Channels[i], config.OutputDir, 0)
		}

		monitorStatus[config.Channels[i].ChannelID] = Status{Live: live/*, Updated: time.Now()*/}
	}
}
