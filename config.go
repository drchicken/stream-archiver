package main

import (
	"errors"
	"github.com/BurntSushi/toml"
	"time"
)

type Config struct {
	ScanInterval string `toml:"interval"`
	OutputDir    string `toml:"output_dir"`

	PostHook       string `toml:"post_hook"`
	PostHookEnable bool   `toml:"post_hook_enabled"`

	TwitchClientID string `toml:"twitch_client_id"`
	TwitchAppToken string `toml:"twitch_app_token"`

	YouTubeCookieFile string	`toml:"youtube_cookies_file"`

	MaxRetries int    `toml:"max_retries"`
	RetryDelay string `toml:"retry_delay"`

	Channels []Channel `toml:"channels"`
}

type Channel struct {
	Name      string `toml:"name"`
	ChannelID string `toml:"id"`
	Type      string `toml:"type"`
}

func LoadConfig() (Config, error) {
	var config Config

	_, err := toml.DecodeFile("config.toml", &config)
	if err != nil {
		return Config{}, err
	}

	// make sure all the channels have (at least) an ID and type
	for i := 0; i < len(config.Channels); i++ {
		if config.Channels[i].ChannelID == "" || config.Channels[i].Type == "" {
			return Config{}, errors.New("channel is missing a required field")
		}

		if config.Channels[i].Name == "" {
			config.Channels[i].Name = config.Channels[i].ChannelID
		}
	}

	maxRetryCount = config.MaxRetries
	retryDelay, err = time.ParseDuration(config.RetryDelay)
	if err != nil {
		return Config{}, err
	}

	return config, nil
}
