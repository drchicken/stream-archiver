# Documentation
> it's probably not that good, I'm sorry.

## Usage
Once built, the program can be ran by running `./stream-archiver`.
* Ensure the command is run in the same folder as a valid `config.toml`
* No built in daemon mode, use `tmux` or `screen` to keep it running
* No command line options, everything is in the config file

## Configuration file
The config is read from a file called `config.toml`. An annotated example can be found in the repo as `config.example.toml`

### Timing & retrying

* `interval` - the interval on which channels should be checked.
  * examples: `1m` for 1 minute, `5m`, for 5 minutes `1m30s` for 1 minute 30 seconds
* `max_retries` - the amount of times an attempt should be made to download a stream.
* `retry_delay` - the delay between retries.
  * examples: `1m` for 1 minute, `5m`, for 5 minutes `1m30s` for 1 minute 30 seconds

### Output directory

* `output_dir` - the location streams should be saved to.
  * Has template options that can be used to create the final path:
  * `{{.Type}}` - `youtube` or `twitch`
  * `{{.Name}}` - channel name as defined in the configuration
  * `{{.ChannelID}}` - the channel's ID 
  * `{{.Date}}` - can be formatted, default is YYYYmmdd. [see this article for reference](https://programming.guide/go/format-parse-string-time-date-example.html)
  * example: `./videos/{{.Date.Format \"20060102\" }}/{{.Type}}/{{.Name}}-{{.ChannelID}}/`

### Twitch API credentials

* `twitch_client_id` - client id from [the twitch developer console](https://dev.twitch.tv/console)
* `twitch_app_token` - app token, can be created (given a valid client id and secret) using [the twitch-cli tool](https://github.com/twitchdev/twitch-cli)

### Hooks

* `post_hook_enable` - `true` or `false` - wether or not to run the hook script
* `post_hook` - path to the script/binary to be executed
See further down in the document for specifics on the hook data

### Channels

It is important to correctly spell `channels` in the config, otherwise the application will not parse them correctly. Channel entries should look like the following:

```toml
[[channels]]
name = "Channel Name for Displaying"
id = "Channel ID"
type = "youtube or twitch"
```
> ensure "type" is set to either "twitch" or "youtube". ***the configuration is case sensitive!***

## Hook variables
Data is passed to hooks in the form of environment variables. The main variable used is `EVENT`, which indicated what event has occured. The types of events are: `DOWNLOAD_START`, `DOWNLOAD_FAIL`, `DOWNLOAD_RETRY`, `DOWNLOAD_META_FAIL`, `DOWNLOAD_FINISH`, `STREAM_START`, `STREAM_END`, and `STREAM_HICCUP`

### Download Start / `DOWNLOAD_START`
* `VIDEO_ID` - id of the stream being downloaded
* `VIDEO_TITLE` - title of the stream being downloaded
* `VIDEO_THUMBNAIL_URL` - thumbnail url for the stream
* `VIDEO_UPLOADER` - streamer's account name, as shown on the target platform
* `VIDEO_UPLOADER_ID` - streamer's account id
* `VIDEO_FILENAME` - expected output file name
* `VIDEO_URL` - link to the stream
* `VIDEO_PLATFORM` - `twitch` or `youtube`

### Download Fail / `DOWNLOAD_FAIL`
* `ERROR_MESSAGE` - short error message
* `VIDEO_ID` - id of the stream being downloaded
* `VIDEO_TITLE` - title of the stream being downloaded
* `VIDEO_THUMBNAIL_URL` - thumbnail url for the stream
* `VIDEO_UPLOADER` - streamer's account name, as shown on the target platform
* `VIDEO_UPLOADER_ID` - streamer's account id
* `VIDEO_FILENAME` - expected output file name
* `VIDEO_URL` - link to the stream
* `VIDEO_PLATFORM` - `twitch` or `youtube`

### Download Retry / `DOWNLOAD_RETRY`
* `RETRY_COUNT` - current download attempt (1 indexed)
* `VIDEO_ID` - id of the stream being downloaded
* `VIDEO_TITLE` - title of the stream being downloaded
* `VIDEO_THUMBNAIL_URL` - thumbnail url for the stream
* `VIDEO_UPLOADER` - streamer's account name, as shown on the target platform
* `VIDEO_UPLOADER_ID` - streamer's account id
* `VIDEO_FILENAME` - expected output file name
* `VIDEO_URL` - link to the stream
* `VIDEO_PLATFORM` - `twitch` or `youtube`

### Download Metadata Fail / `DOWNLOAD_META_FAIL`
* `ERROR_MESSAGE` - short error message
* `CHANNEL_NAME` - channel name, as set in the config
* `CHANNEL_ID` - channel id, as set in the config
* `CHANNEL_TYPE` - `twitch` or `youtube`

### Download Finish / `DOWNLOAD_FINISH`
* `VIDEO_ID` - id of the stream being downloaded
* `VIDEO_TITLE` - title of the stream being downloaded
* `VIDEO_THUMBNAIL_URL` - thumbnail url for the stream
* `VIDEO_UPLOADER` - streamer's account name, as shown on the target platform
* `VIDEO_UPLOADER_ID` - streamer's account id
* `VIDEO_FILENAME` - expected output file name
* `VIDEO_URL` - link to the stream
* `VIDEO_PLATFORM` - `twitch` or `youtube`
* `FILE_FULLPATH` - full path to the saved file
* `FILE_THUMBNAIL` - full path the the saved thumbnail

### Stream Start / `STREAM_START`
> fired before the download is started

* `CHANNEL_NAME` - channel name, as set in the config
* `CHANNEL_ID` - channel id, as set in the config
* `CHANNEL_TYPE` - `twitch` or `youtube`

### Stream End / `STREAM_END`
> fired when the monitor detects a channel is no longer live, independently of "Download Finish"

* `CHANNEL_NAME` - channel name, as set in the config
* `CHANNEL_ID` - channel id, as set in the config
* `CHANNEL_TYPE` - `twitch` or `youtube`

### Stream Hiccup / `STREAM_HICCUP`
> fired when a download ends, but the monitor still detects a running stream

* `CHANNEL_NAME` - channel name, as set in the config
* `CHANNEL_ID` - channel id, as set in the config
* `CHANNEL_TYPE` - `twitch` or `youtube`