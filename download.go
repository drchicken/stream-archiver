package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"time"
	"strings"
)

type OutputDirData struct {
	*Channel
	Metadata *VideoMetadata
	Date time.Time
}

func download(channel Channel, outputDir string, try int) {
	downloadingStatus[channel.ChannelID] = true

	log.Printf("[downloader] collecting metadata from %s (%s)\n", channel.Name, channel.Type)

	meta, err := getVideoMetadata(channel, metadataTimeout)
	if err != nil {
		// TODO: retry on metadata collection?
		log.Printf("[downloader] failed to collect metadata from %s (%s): %v\n", channel.Name, channel.Type, err)
		onDownloadMetaFail(channel, err.Error())
		downloadingStatus[channel.ChannelID] = false
		return
	}

	// twitch has extra metadata gathering steps
	if channel.Type == "twitch" {
		extraMeta, err := twGetMetadata(channel.ChannelID)
		// TODO: retry on metadata collection?
		if err != nil {
			log.Printf("[download] failed to collect api metadata from %s: %v\n", channel.Name, err)
			onDownloadFail(meta, "failed to collect twitch api metadata")
			downloadingStatus[channel.ChannelID] = false
			return
		}

		cleanTitle := strings.ReplaceAll(extraMeta.Title, "/", "_")

		meta.Title = extraMeta.Title
		meta.Filename = fmt.Sprintf("%s - %s - %s.mp4", time.Now().Format("20060102"), extraMeta.UserLogin, cleanTitle)
	}

	onDownloadStart(meta)

	// TODO: we have metadata, why not use it here?
	var dirB bytes.Buffer
	data := OutputDirData{
		Channel: &channel,
		Metadata: &meta,
		Date:    time.Now(),
	}
	outputDirTemplate.Execute(&dirB, data)

	dir := path.Clean(dirB.String())

	os.MkdirAll(dir, 0777)


	thumbPath := fmt.Sprintf("%s%s", strings.TrimSuffix(meta.Filename, path.Ext(meta.Filename)), path.Ext(meta.Thumbnail))
	thumb, err := os.Create(path.Join(dir, thumbPath))
	if err != nil {
		log.Printf("[downloader] failed to create thumb for %s (%s): %v\n", channel.Name, channel.Type, err)
		onDownloadFail(meta, "failed to create thumbnail file")
		downloadingStatus[channel.ChannelID] = false
		return
	}

	resp, err := http.Get(meta.Thumbnail)
	if err != nil {
		log.Printf("[downloader] failed to download thumb for %s (%s): %v\n", channel.Name, channel.Type, err)
		onDownloadFail(meta, "failed to download thumbnail")
		downloadingStatus[channel.ChannelID] = false
		return
	}

	_, err = io.Copy(thumb, resp.Body)
	if err != nil {
		log.Printf("[downloader] failed to write thumb for %s (%s): %v\n", channel.Name, channel.Type, err)
		onDownloadFail(meta, "failed to write thumbnail data")
		downloadingStatus[channel.ChannelID] = false
		return
	}

	resp.Body.Close()
	thumb.Close()

	var url string
	if channel.Type == "youtube" {
		url = fmt.Sprintf("https://www.youtube.com/channel/%s/live", channel.ChannelID)
	} else if channel.Type == "twitch" {
		url = fmt.Sprintf("https://www.twitch.tv/%s", channel.ChannelID)
	} else {
		panic("unkown channel type")
	}

	log.Printf("[downloader] starting download from %s (%s)\n", channel.Name, channel.Type)

	// create destination directory
	// yt-dlp does this automatically, but streamlink doesn't
	err = os.MkdirAll(dir, 0777)
	if err != nil {
		log.Printf("[downloader] failed to create output directory for %s from %s (%s)\n", meta.Title, meta.Uploader, meta.UploaderID)
		onDownloadFail(meta, "failed to create output directory")
		return
	}

	var cmd *exec.Cmd

	if channel.Type == "youtube" {
		if config.YouTubeCookieFile != "" {
			// a cookie file was specified, make sure yt-dlp knows about it
			cmd = exec.Command("yt-dlp", "-P", dir, "-o", meta.Filename, "-ciw", "--add-metadata", "--hls-use-mpegts", "--no-part", url)
		} else {
			cmd = exec.Command("yt-dlp", "--cookies", config.YouTubeCookieFile,"-P", dir, "-o", meta.Filename, "-ciw", "--add-metadata", "--hls-use-mpegts", "--no-part", url)
		}
	} else if channel.Type == "twitch" {
		cmd = exec.Command("streamlink", "--force", "--twitch-disable-hosting", "-o", path.Join(dir, meta.Filename), url, "best")
	}

	logFile, err := os.Create(fmt.Sprintf("./logs/%s-%s-%d.log", channel.Type, channel.ChannelID, time.Now().Unix()))
	if err != nil {
		log.Printf("[downloader] failed to open log file: %v\n", err)
		onDownloadFail(meta, "failed to open log file")
		downloadingStatus[channel.ChannelID] = false
		return
	}

	cmd.Stdout = logFile
	cmd.Stderr = logFile

	err = cmd.Run()
	if err != nil {
		var ee *exec.ExitError
		var pe *os.PathError

		if errors.As(err, &ee) {
			log.Printf("[downloader] yt-dlp failed to download the video for %s (%s): %v\n", channel.Name, channel.Type, ee.String())
			onDownloadFail(meta, fmt.Sprintf("failed to download video: %s", ee.String()))
		} else if errors.As(err, &pe) {
			log.Printf("[downloader] couldn't invoke yt-dlp: %v\n", pe.Error())
			onDownloadFail(meta, fmt.Sprintf("couldn't invoke yt-dlp: %s", pe.Error()))
		} else {
			log.Printf("[downloader] an error occured while trying to download video for %s (%s): %v\n", channel.Name, channel.Type, err)
			onDownloadFail(meta, fmt.Sprintf("unknown error: %s", err.Error()))
		}

		// increment try
		try++

		// check if we're under the retry limit
		if try > maxRetryCount {
			// give up
			log.Printf("[downloader] giving up on channel %s (%s)\n", channel.Name, channel.Type)
			onDownloadFail(meta, "hit retry limit")
			downloadingStatus[channel.ChannelID] = false
			return
		}

		log.Printf("[downloader] retrying %s (%s) in %s...\n", channel.Name, channel.Type, retryDelay.String())
		onDownloadRetry(meta, try)
		time.Sleep(retryDelay)
		download(channel, outputDir, try)
		return
	}

	log.Printf("[downloader] %s (%s) download has finished\n", channel.Name, channel.ChannelID)

	// downloading is done, execute the hook
	downloadingStatus[channel.ChannelID] = false
	time.Sleep(5 * time.Second)

	// fill these in here, since the hook functions don't have access to the config
	meta.FilePath = path.Join(dir, meta.Filename)
	meta.ThumbnailPath = path.Join(dir, thumbPath)

	onDownloadFinish(meta)
}
