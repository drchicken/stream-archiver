module git.disroot.org/drchicken/stream-archiver

go 1.17

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/MercuryEngineering/CookieMonster v0.0.0-20180304172713-1584578b3403 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/nicklaw5/helix v1.25.0 // indirect
	github.com/robfig/cron/v3 v3.0.1 // indirect
)
