package main

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"log"
	"os"
	"text/template"
	"time"
)

var (
	outputDirTemplate *template.Template
	maxRetryCount   int
	retryDelay      time.Duration
	metadataTimeout = 15 * time.Second // TODO: fine tune

	config Config
)

func main() {
	var err error
	config, err = LoadConfig()
	if err != nil {
		log.Fatalf("failed to load config: %v\n", err)
	}

	// make sure logs directory exists
	err = os.MkdirAll("./logs/", 0777)
	if err != nil {
		panic(err)
	}

	// set up twitch
	setupTwitch(config)

	// set up youtube-specific bits
	setupYouTube()

	// compile template
	outputDirTemplate = template.Must(template.New("outputdir").Parse(config.OutputDir))

	log.Printf("monitoring %d channels\n", len(config.Channels))

	monitor(config)

	c := cron.New()
	// TODO: not hardcode this!
	c.AddFunc(fmt.Sprintf("@every %s", config.ScanInterval), func() { monitor(config) })
	c.Start()

	select {}
}
